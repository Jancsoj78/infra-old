#!/bin/bash --
#http//360percents.com/
domain=`ifconfig | grep 'Bcast:' | cut -d ":" -f 2 | cut -d "." -f 1,2,3`
#script needs root privileges to perform ICMP scan

if [ "$(id -u)" != "0" ]; then
   echo "This script needs root privileges, exiting..." 1>&2
      exit 1
      fi
      
#      domain='192.168.0';
      
      #ping all subnet hosts /24
      hosts=`nmap $domain.0/24 -sP | grep 'report' | sed -e "s/.*$domain.//" -e 's/)//g'`
      
      for host in $hosts;
      do
    	name=`host $domain.$host | grep 'domain' | awk '{ print $NF }' | cut -d '.' -f 1,2,3`
    		echo "$domain.$host:$name";
    			online[`expr ${#online[@]} + 1`]=$host;
    			done
    			
    			echo "Computers online: ${#online[*]}";
    			
    			exit