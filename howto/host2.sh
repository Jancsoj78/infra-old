#!/bin/bash --
#http//360percents.com/
#domain='xxx.xxx.xxx';

domain=`ifconfig | grep 'Bcast:' | cut -d ":" -f 2 | cut -d "." -f 1,2,3`

#ping all subnet devices /24
for host in `seq 0 255`;
do
    ip=`ping -w 1 -c 1 $domain.$host | tr -d "\-\-\-\n" | grep '1 received' | cut -d ' ' -f 2`
	if [ -n "$ip" ];
	    then
		    name=`host $ip | grep 'domain' | awk '{ print $NF }' | cut -d '.' -f 1,2,3`
			    echo "$ip:$name";
				    online[`expr ${#online[@]} + 1`]=$host;
					fi
					done
					
					echo "Hosts online: ${#online[*]}";