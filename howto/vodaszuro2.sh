#!/bin/bash

#script ami vodafone csv állományokból leszűri bemenő lista alapján mely számok vannak benne
#illetve melyek nem | 2013-03-05 | euromacc.hu | Jancsó József

# sortörések kezelése

    dos2unix szamok

# létező számok kiszűrése elérési úttal (duplikátumot tartalmaz) - vantmp0

    while read file 
    do 
{
find . -name '*.csv' -exec grep -H $file  \{\} \; >> vantmp0
}
done < szamok

# van.csv előállítása
#csak BTEL!

#________________________________________________________

sed -n 's/[^BTel]*//p' vantmp0  > vantmp00 # elérési út törlése
sed -i 's/S;P;/WAIT/g' vantmp00     # WAIT cseréje
sed -i 's/;/,/g' vantmp00  # ;cseréje
sed -i 's/:/,/g' vantmp00  # : cseréje
awk -F "," '{print $3",",$4",",$5",",$1}' ./vantmp00 >van.csv


#________________________________________________________



# kiszürt számok és eredeti lista összevetése - nincs.txt

sed -n 's/[^;]*;//p' vantmp0 >vantmp1
sed -n 's/[^;]*;//p' vantmp1 >vantmp2
cut -c -9 vantmp2 >vantmp3
sort vantmp3 |uniq > van_csakszam # duplikátumok kiszedve

rm ./vantmp*

grep -v -f van_csakszam szamok >nincstmp
sort nincstmp | uniq > nincs.csv  # duplikátumok kiszedve

rm ./nincstmp*

# az eredmény előáll a van.csv be és a nincs.csv -ben.
# good luck!
