@ECHO off
@REM a c:\msi könyvtárba rakd
@REM a c:\msi\cpkgout ban van a script.cmd azt kell futtatni
@REM a c:\msi könyvtárban lesznek a cimkézett msi -k.
@REM a cimkefájl a c:\msi\cpkgout\label.txt


@REM start
For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set mytime=%%a%%b)
echo start: %mytime%

cd c:\msi
rmdir /S /Q msixmlout

@REM alap MSI kicsomagolasa
@REM ez az az MSI amit klónozunk

copy "SAM-Insights ClientPack3.msi" "SAM-Insights ClientPack3.msi-old"
msi2xml -c msixmlout "SAM-Insights ClientPack3.msi"

@ REM MSI klónozás
cd c:\msi\cpkgout
rmdir /S /Q out
mkdir out

@REM
SETLOCAL EnableDelayedExpansion
SET pwd=*432*xsw*321*YAQ

@REM  ez a configuration.cp3pkg fájl az MSI-ben
@REM a fájlméret alapján lehet megtalálni a kicsomagolt msi ben.

SET cp3pkgfile=..\..\msixmlout\_2A792C10757ADC55EFF0FD2CFF9DABFC
SET csere=c:\msi\msixmlout\_2A792C10757ADC55EFF0FD2CFF9DABFC

@REM mentjuk az eredeti cp3pkg -t !!
copy %csere% c:\msi\tmp.tmp 

rmdir /S /Q out
mkdir out

for /f "delims=" %%x in (label.txt) do ( 

rmdir /S /Q out
mkdir out

cd out && ..\7za x -p%pwd% %cp3pkgfile% && cd ..

fart -Vc c:\msi\cpkgout\out\* GM_04_[computer]_[mac] %%x_[computer]_[mac]
cd out && ..\7za a -p%pwd% ..\%%x.zip
copy ..\%%x.zip %cp3pkgfile%
cd c:\msi

xml2msi -m "SAM-Insights ClientPack3.xml"
move "SAM-Insights ClientPack3.MSI" c:\msi\clients\SAM-Insights_ClientPack3-%%x.msi

@REM visszamasoljuk az eredeti cp3pkg -t !!

copy c:\msi\tmp.tmp %csere%


cd c:\msi\cpkgout
)

@REM takaritas
cd c:\msi\cpkgout
rmdir /S /Q out
rmdir /S /Q ..\msixmlout
del *.zip
del ..\*.xsl ..\*.xml
cd c:\msi
rename "SAM-Insights ClientPack3.msi-old" "SAM-Insights ClientPack3.msi"
del tmp.tmp

For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set mytime=%%a%%b)
echo end %mytime%

@REM end