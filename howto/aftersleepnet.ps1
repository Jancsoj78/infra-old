powershell;
Disable-NetAdapter ethernet -Confirm:$false ; 
Get-NetAdapter ethernet ; 
Enable-NetAdapter ethernet -Confirm:$false ; 
Start-Sleep -s 4 ; 
Get-NetAdapter ethernet;
exit;