
#!/bin/bash
# datum forgatáshoz
fecha=`date +"%d-%m-%Y"`
# Backup and gzip the directory (dokumentumok)

#tar -cpvf - /opt/samba/ | pigz > /backup/tar/dunaprec_tar-$fecha.tgz

# Backup and gzip the directory (emails)

tar -cpvf - /home/dunaprec/mail/ | pigz > /backup/mail/dunaprec_mail-$fecha.tgz


# LDAP export (cimtar)

/usr/sbin/slapcat -l /backup/cimtar/dunaprec-$fecha.ldif


# Rotate backup , delete older than 2-3 days

#find /backup/tar -mtime +0 -exec rm {} \;
find /backup/mail -mtime +1 -exec rm {} \;
find /backup/cimtar -mtime +1 -exec rm {} \;

# send status riport

cd /root
./status_report.sh
