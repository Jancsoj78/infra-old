#!/bin/bash

##########################################################################
# create-staging-db.sh
#
# Usage:
#   ./create-staging-db.sh [instance_name]
#
# Creates a new RDS instance by cloning the latest production snapshot.
# More specifically, the following steps are performed:
#   - Determine the snapshot id to use
#   - Delete the existing database
#   - Create the new database
#   - Make necessary modifications to the new instances (disable backups)
##########################################################################


instance_identifier=$1
#instance_class=db.t2.micro
#security_group=sg-XXXXXXXX
#subnet_group=default-vpc-XXXXXXXX

snapshot_identifier="lvl-db-prod"
#instance_identifier="lvl-db-staging-3"
instance_class="db.t2.small"
security_group="sg-d0d9f8a8"
subnet_group="default"

function wait-for-status {
    instance=$1
    target_status=$2
    status=unknown
    while [[ "$status" != "$target_status" ]]; do
        status=`aws rds describe-db-instances \
            --db-instance-identifier $instance | head -n 1 \
            | awk -F \  '{print $11}'`
        sleep 5
    done
}

function wait-until-deleted {
    instance=$1
    count=1
    while [[ "$count" != "0" ]]; do
        count=`aws rds describe-db-instances \
            --db-instance-identifier $instance 2>/dev/null \
            | grep DBINSTANCES \
            | wc -l`
        sleep 5
    done
}

# fetch snapshot id
snapshot_id=`aws rds describe-db-snapshots \
    --db-instance-identifier $snapshot_identifier \
    | tail -n 1 \
    | awk -F \  '{print $5}'`

echo "Snapshot Id: $snapshot_id"
echo " Deleting database (if exists): $instance_identifier"

# delete the existing instance
aws rds delete-db-instance \
    --db-instance-identifier $instance_identifier \
    --skip-final-snapshot > /dev/null 2>&1

wait-until-deleted $instance_identifier

echo "Creating new database: $instance_identifier"

# create the new instance
aws rds restore-db-instance-from-db-snapshot \
    --db-instance-identifier $instance_identifier \
    --db-snapshot-identifier=$snapshot_id \
    --db-instance-class $instance_class \
    --storage-type gp2 \
    --publicly-accessible \
    --no-multi-az \
    --no-auto-minor-version-upgrade \
    --db-subnet-group-name $subnet_group > /dev/null

echo "Waiting for new DB instance to be available"

wait-for-status $instance_identifier available

echo "New instance is available"
echo "Disabling backup retention"

# disable backup retention
aws rds modify-db-instance \
    --db-instance-identifier $instance_identifier \
    --backup-retention-period 0 \
    --apply-immediately

echo "Waiting for new DB instance to be available"

wait-for-status $instance_identifier available

echo "New instance is available"
echo "Clone process is copmlete"