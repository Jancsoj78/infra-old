function cloneinstance {
  awsinstanceid=$1
  region=$2
  export AWS_DEFAULT_REGION=$region
  ami=$(aws ec2 describe-instances --instance-ids $awsinstanceid | grep INSTANCES | awk '{print $6}')
  privatekey=$(aws ec2 describe-instances --instance-ids $awsinstanceid | grep INSTANCES | awk '{print $9}')
  securitygroup=$(aws ec2 describe-instances --instance-ids $awsinstanceid | grep SECURITYGROUPS | awk '{print $2}')
  instancetype=$(aws ec2 describe-instances --instance-ids $awsinstanceid | grep INSTANCES | awk '{print $8}')
  subnet=$(aws ec2 describe-instances --instance-ids $awsinstanceid | grep NETWORKINTERFACES | awk '{print $9}')

  awsinstancedata=$(aws ec2 run-instances --image-id $ami --key-name $privatekey --security-group-ids $securitygroup --instance-type $instancetype --subnet-id $subnet)
  awsinstanceid=$(echo $awsinstancedata | awk '{print $9}')

  # AWS CLI sucks and doesn't return error codes so have to look for a valid id
  if [[ "$awsinstanceid" == i-* ]]; then echo -e "\t\tSuccessfully created. Instance ID: $awsinstanceid"; else echo -e "\t\tSomething went wrong. Check your configuration."; exit 1; fi 
  echo -e "\t\tWaiting for it to come up..."
  aws ec2 wait instance-running --instance-ids $awsinstanceid
  echo -e "\t\tServer is up and ready"
}

#cloneinstance i-0c8bb9c6077f50a90 eu-west-1
#cloneinstance i-978fef71 eu-west-1

echo from ami: $ami and instance: $awsinstanceid
