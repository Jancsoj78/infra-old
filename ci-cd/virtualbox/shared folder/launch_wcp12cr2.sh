#!/bin/bash

# start weblogic
cd /oracle/scripts/
sudo -H -u oracle ./startIPR2.sh

#weblogic start time ..open port check....

 now=$(date +"%T")
    echo -ne "Starting time : $now "
    echo waiting for port 7001 ...

until nc -z 127.0.0.1 7001;
do
    now=$(date +"%T")
    echo -ne "Current time  : $now \r".

    sleep 1

done

#samscan

cd /media/sf_test_cases/scripts/samscan/
bash ./samscan2_9.sh

exit 0
