#!/bin/bash

# start weblogic

nohup /opt/oracle/Middleware/Oracle_Home/user_projects/domains/base_domain/bin/startWebLogic.sh &
nohup sudo -H -u oracle /opt/oracle/Middleware/Oracle_Home/user_projects/domains/base_domain/bin/startNodeManager.sh &
nohup sudo -H -u oracle /opt/oracle/Middleware/Oracle_Home/user_projects/domains/base_domain/bin/startManagedWebLogic.sh Server-0 t3://127.0.0.1:7001 &

#weblogic start time ..open port check....

 now=$(date +"%T")
    echo -ne "Starting time : $now "
    echo waiting for port 7003 ...

until nc -z 127.0.0.1 7003;
do
    now=$(date +"%T")
    echo -ne "Current time  : $now \r".

    sleep 1

done

#samscan

cd /media/sf_test_cases/scripts/samscan/
bash ./samscan2_9.sh

exit 0
