@echo off

rem ----------------------------
rem  IPR Virtualbox Automatization Script
rem  DOS script  fájlból (machines.txt) loop, eredményfájl ellenőrzéssel, ha létezik akkor megy tovább
rem jjancso  | IPR |  2016.12.07 | v 1.0
rem -----------------------------
cls
setlocal enableDelayedExpansion
set LookForFile=
set foo=

echo script start
chcp 65001
echo. 

echo test started >log.txt
echo !TIME! >>log.txt

for /f "tokens=*" %%a in (machines.txt) do (

echo.
echo info: következő gép
echo.

set LookForFile=d:\vms\test_cases\scripts\samscan\%%a.xml
set foo=%%a
echo gépnév: %%a

echo. >>log.txt
echo ---------------------- >> log.txt
echo.

echo %%a start >>log.txt
echo !TIME! >>log.txt

	"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" showvminfo "%%a" --machinereadable |grep description >>log.txt
	"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm "%%a" poweroff
	"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm "%%a" --type headless

	ipconfig /flushdns

rem -------------------------
rem  eredményfájl ellenőrzése
rem -------------------------
call :CheckForFile

)

echo.
echo script finish

echo ---------------------- >> log.txt
echo. >>log.txt

echo test finished>>log.txt
echo !TIME! >>log.txt

endlocal
exit /b

:CheckForFile

echo Eredményfájl név : %LookForFile%

if exist %LookForFile% (

echo Found: %LookForFile%
ping %foo% -n 1 >>log.txt

echo %foo% scan finished vm stopped

"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm %foo% poweroff
exit /b	
)
timeout /T 5 >nul
echo eredményfájlra várva....
call :CheckForFile
exit /b