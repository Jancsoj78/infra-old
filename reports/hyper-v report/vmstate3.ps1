# VM monitoring script |  sorted uptime backward
#Name        : 
#Type        : 
#Status      : 
#Uptime      : 
#Ipaddresses : 
#Path        : 
#Total(GB)   :
#Used(GB)    : 
#Free(GB)    :

$VMs = get-vm | sort Uptime -Descending 

# state filter
#| ?{$_.State -eq "Running"} 
#| ?{$_.State -eq "off"} 

echo $vm.computername " VM state Generated at:" $a
$a = Get-Date
"Date: " + $a.ToShortDateString()
"Time: " + $a.ToShortTimeString()

echo "_____________...sorted uptime descending...___________________________"

foreach ($VM in $VMs){
		
		$VHDs=Get-VHD $VM.harddrives.path -ComputerName $vm.computername
		$VMs2 = GET-VM $VM.vmname | select -ExpandProperty networkadapters | select ipaddresses, switchname
			
			foreach ($VHD in $VHDs) {
                New-Object PSObject -Property @{
                    Name = $VM.name
                    State = $VM.State
					Type = $VHD.VhdType
					Ipaddresses = $VMs2.ipaddresses
					Switchname = $VMs2.switchname
					Path = $VHDs.path
					Uptime = $VM.uptime
					Status = $VM.status
                    'Total(GB)' = [math]::Round($VHD.Size/1GB)
                    'Used(GB)' = [math]::Round($VHD.FileSize/1GB)
                    'Free(GB)' =  [math]::Round($VHD.Size/1GB- $VHD.FileSize/1GB)
				 } |select Name, State, Type, Status, Uptime, Ipaddresses,switchname, Path, 'Total(GB)', 'Used(GB)', 'Free(GB)' 
    }
 }
#sysadmin | jjancso | 06.2015