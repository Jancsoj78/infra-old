@echo off
echo ____________________________________
echo.
rem ipcheck script by jancsoj78@gmail.com

rem date
for /F "usebackq tokens=1,2 delims==" %%i in (`wmic os get LocalDateTime /VALUE 2^>NUL`) do if '.%%i.'=='.LocalDateTime.' set ldt=%%j
set ldt=%ldt:~0,4%-%ldt:~4,2%-%ldt:~6,2% %ldt:~8,2%:%ldt:~10,2%:%ldt:~12,6%
echo [%ldt%]

rem hostname
FOR /F "usebackq" %%i IN (`hostname`) DO SET MYVAR=%%i
ECHO VM name %MYVAR%

rem ip
set ip_address_string="IP Address"
rem Uncomment the following line when using Windows 7 (with removing "rem")!
set ip_address_string="IPv4 Address"

for /f "usebackq tokens=2 delims=:" %%f in (`ipconfig ^| findstr /c:%ip_address_string%`) do echo VM IP: %%f

setlocal ENABLEEXTENSIONS
set KEY_NAME="HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Virtual Machine\Guest\Parameters"
set VALUE_NAME=PhysicalHostName

FOR /F "tokens=3" %%A IN ('REG QUERY %KEY_NAME% /v %VALUE_NAME% 2^>nul') DO (
   set ValueName=%%A
 REM  set ValueType=%%B
 REM   set ValueValue=%%C
)

if defined ValueName (
    @echo Hosted by = %ValueName%
 REM   @echo Value Type = %ValueType%
 REM   @echo Value Value = %ValueValue%
) else (
    @echo %KEY_NAME%\%VALUE_NAME% not found.
)
echo ____________________________________
