#automatic IPR SAMI Databases Dashboard
#scripts library
# last modification : 03.06.2018 (JJ)

my $version = 1.0;

use CGI::Carp qw(warningsToBrowser fatalsToBrowser);
use warnings;
use DBI;
use Time::HiRes qw(gettimeofday tv_interval);
use encoding "utf-8";
#use strict;

print "Content-type: text/html\n\n\n";
print '<!DOCTYPE html>';
print '<html lang="en">';
print '  <meta charset="utf-8">';
print '  <meta name="description" content=""> ';
print '  <meta name="author" content="">';
print '  <meta name="viewport" content="width=device-width, initial-scale=1">';
print '  <head> ';
print '  <title>Főnök kész van a kimutatás!</title>';

print '<style> a:hover:after{ content: attr(data-tooltip)} </style>';

print '<style>';
print '#customers {';
print '    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;';
print '    border-collapse: collapse;';
print '    width: 100%;';
print '}';

print '#customers td, #customers th {';
print '    border: 1px solid #ddd;';
print '    padding: 8px;';
print '}';

print '#customers tr:nth-child(even){background-color: #f2f2f2;}';

print '#customers tr:hover {background-color: #add;}';

print '#customers th {';
print '    padding-top: 12px;';
print '    padding-bottom: 12px;';
print '    text-align: left;';
print '    background-color: #4CAF50;';
print '    color: white;';
print '}';

print '.button {';
print '    background-color: #4CAF50; /* Green */';
print '    border: none;';
print '    color: white;';
print '    padding: 15px 32px;';
print '    text-align: center;';
print '    text-decoration: none;';
print '    display: inline-block;';
print '    font-size: 16px;';
print '    margin: 4px 2px;';
print '    cursor: pointer;';
print '}';


print '</style>';
print '</head><body>';

use CGI;
use encoding "utf-8";
my $query = CGI->new;

my $server = $query->param( 'server' );
my $db = $query->param( 'db' );

$t0 = [gettimeofday];

print "<h4>Unkown files with header and vendors on $server and on $db database</h4>";

MySQLs1("select fpsw.cCompanyName as [Vendor], count( distinct fp.uID) as [Unknown files with header of important vendors] 
from dbo.SamDataSetFilePrint sdsfp (nolock)
join dbo.FilePrint fp (nolock) on fp.uID = sdsfp.uFilePrintID
join dbo.FilePrintSW fpsw (nolock) on fpsw.uID = fp.uFilePrintSWID
where fp.bDontCare = 0 and fp.bUnknownMarked = 0
  and fpsw.uID <> 'A3E0FC04-7000-4F9A-9F77-D69C63687AD9' --Not the technical header uID!
  and ( fpsw.cCompanyName  like ('%Microsoft%') or              
	    fpsw.cCompanyName  like ('%SAP%') or
		fpsw.cCompanyName  like ('%Citrix%') or
		fpsw.cCompanyName  like ('%Cisco%') or
		fpsw.cCompanyName  like ('%Avaya%') or
		fpsw.cCompanyName  like ('%Printsoft%') or
		fpsw.cCompanyName  like ('%Mcafee%') or
		fpsw.cCompanyName  like ('%IBM%') or
		fpsw.cCompanyName  like ('%International business%') or 
		fpsw.cCompanyName  like ('%Dell%') or 
		fpsw.cCompanyName  like ('%ADOBE%') or
		fpsw.cCompanyName  like ('%Corel%') or
		fpsw.cCompanyName  like ('%Autodesk%') or
		fpsw.cCompanyName  like ('%Symantec%') or
		fpsw.cCompanyName  like ('%oracle%') or
		fpsw.cCompanyName  like ('%VmWare%') or
		fpsw.cCompanyName  like ('%Mozilla%') or
		fpsw.cCompanyName  like ('%Igor Pavlov%') or
		fpsw.cCompanyName  like ('%Macromedia%') )
and not exists ( select 1
				 from dbo.FilePrintOfSWProductVersion (nolock)
				 where uFilePrintID = fp.uID )
and not exists ( select 1
				 from dbo.FilePrintOfSWRecognitionRule (nolock)
				 where uFilePrintID = fp.uID )
and exists ( select 1
             from dbo.Computer c (nolock)
			 where c.uActualSAMDataSetID = sdsfp.uSAMDataSetID
			   and c.cStatus = 'ACTIVE' )
group by fpsw.cCompanyName
order by fpsw.cCompanyName");

MySQLs2("select count( distinct fp.uID) as [Unknown files with header] 
from dbo.SamDataSetFilePrint sdsfp (nolock)
join dbo.FilePrint fp (nolock) on fp.uID = sdsfp.uFilePrintID
join dbo.FilePrintSW fpsw (nolock) on fpsw.uID = fp.uFilePrintSWID
where fp.bDontCare = 0 and fp.bUnknownMarked = 0
  and fpsw.uID <> 'A3E0FC04-7000-4F9A-9F77-D69C63687AD9' --Not the technical header uID!
and not exists ( select 1
				 from dbo.FilePrintOfSWProductVersion (nolock)
				 where uFilePrintID = fp.uID )
and not exists ( select 1
				 from dbo.FilePrintOfSWRecognitionRule (nolock)
				 where uFilePrintID = fp.uID )
and exists ( select 1
             from dbo.Computer c (nolock)
			 where c.uActualSAMDataSetID = sdsfp.uSAMDataSetID
			   and c.cStatus = 'ACTIVE' )");

sub MySQLs1
	{ 			
		my $dbh = DBI->connect("dbi:ODBC:driver={SQL Server};server=$server;database=$db;trusted_connection=yes") or croak "$DBI::errstr\n";
		my $query = $_[0];  #assign argument to string
		my $statement = $dbh->prepare($query);   #prepare query
		$statement->execute();   #execute query	
		print "<h3></h3>";				

		if ($DBI::errstr) { 
		print "<h4> Stored Procedure is not exist.</br> error code:</h4>";
		print "$DBI::errstr\n";
		}
		else {
		print '<table border=1 id="customers">';				
		while (@datasp = $statement->fetchrow_array()) 
		{
				print "<tr><td>" . "$datasp[0]\n" ."</td><td>" . "$datasp[1]\n" . "</td></tr>";	
				$aaa+="$datasp[1]\n";			
		}				
		print "</table>";
		}
		
}	

sub MySQLs2
	{ 			
		my $dbh = DBI->connect("dbi:ODBC:driver={SQL Server};server=$server;database=$db;trusted_connection=yes") or croak "$DBI::errstr\n";
		my $query = $_[0];  #assign argument to string
		my $statement = $dbh->prepare($query);   #prepare query
		$statement->execute();   #execute query	
		print "<h3></h3>";				

		if ($DBI::errstr) { 
		print "<h4> something is wrong </br> error code:</h4>";
		print "$DBI::errstr\n";
		}
		else {
		print "<h4> Unknown files with header: ";				
		while (@datasp = $statement->fetchrow_array()) 
		{
				print "$datasp[0]\n" . " </h4> ";				
		}				
		}
}		
print '<form>'; 
print '<input type="Button" class="button" value="Back" onclick="history.back()">'; 
print '</form>'; 
$elapsed = tv_interval ($t0, [gettimeofday]);
print "<h4>web load time: $elapsed  ms" . " | script version: $version | checking time: " .(localtime) ." \n</h4>";
print '</body></html>';