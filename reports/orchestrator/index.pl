#automatic IPR SAMI Databases Dashboard
#script shows db version, publications other sqls and SIDE2 Connection string

# Written by Jozsef Jancso  (jancsoj78@gmail.com)
# v 1.0 - sort and reverse list
# v 1.1 - add connection strings
# v 1.2 add stored procedure
# v 1.2 run executable test
# v 1.3 IDENT name
#
# last modification : 03.06.2018 (JJ)

my $version = 1.4;
#szinezés 10 nél kisebb piros
my $threshold=10;

use CGI::Carp qw(warningsToBrowser fatalsToBrowser);
use CGI;
use encoding "utf-8";
use warnings;
use DBI;
use Time::HiRes qw(gettimeofday tv_interval);

print "Content-type: text/html\n\n\n";
print '<!DOCTYPE html>';
print '<html lang="en">';
print '  <meta charset="utf-8">';
print '  <meta name="description" content=""> ';
print '  <meta name="author" content="">';
print '  <meta name="viewport" content="width=device-width, initial-scale=1">';
print '  <head> ';
print '  <title>Főnök kész van a kimutatás!</title>';

print '<style> a:hover:after{ content: attr(data-tooltip)} </style>';

print '<style>';
print '#customers {';
print '    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;';
print '    border-collapse: collapse;';
print '    width: 100%;';
print '}';

print '#customers td, #customers th {';
print '    border: 1px solid #ddd;';
print '    padding: 8px;';
print '}';

print '#customers tr:nth-child(even){background-color: #f2f2f2;}';

print '#customers tr:hover {background-color: #add;}';

print '#customers th {';
print '    padding-top: 12px;';
print '    padding-bottom: 12px;';
print '    text-align: left;';
print '    background-color: #4CAF50;';
print '    color: white;';
print '}';
print '</style>';
print '</head><body>';

my @db_all = ("psrvsql01","psrvsql01\\sql2005","psrvsql10","psrvsql10\\sql2012","hupsrvsql001\\sql2014");
my $szaml=scalar @db_all;
my (@all, @data, @datax, @datasp);

sub dbfeltoltes 
{

MySQLi("select * from sys.databases WHERE name NOT IN ('master', 'tempdb', 'model', 'msdb')"); 
sub MySQLi 
	{ 		
	for ( my $i=0; $i <= $szaml-1; $i++) {	
		my $dbh = DBI->connect("dbi:ODBC:driver={SQL Server};server=$db_all[$i];database=master;trusted_connection=yes") or croak "$DBI::errstr\n";
		my $query = $_[0];  #assign argument to string
		my $statement = $dbh->prepare($query);   #prepare query
		$statement->execute();   #execute query	
		
			while (@data = $statement->fetchrow_array()) 
		{
		
	# sql pub and version
	MySQLu ("SELECT Cvalue, t.nPublicationNumber,(SELECT top 1 cValue FROM [dbo].[ConfigSettings] where cKey='IDENTNOW') as ident FROM dbo.configsettings,(SELECT TOP 1 nPublicationNumber FROM dbo.publish order by nPublicationNumber desc) t where cKey ='VersionInfo'");

	sub MySQLu 
				{
	
		$all[$x][0] = $db_all[$i];
		$all[$x][1] = $data[0];

			my $dbh = DBI->connect("dbi:ODBC:driver={SQL Server};server=$db_all[$i];database=$data[0];trusted_connection=yes") or croak "$DBI::errstr\n";
			my $query = $_[0];  #assign argument to string
			my $statement = $dbh->prepare($query);   #prepare query
			$statement->execute();   #execute query	
	
			while (@datax = $statement->fetchrow_array()) 
					{
					
						$all[$x][2] = "$datax[0]" ;
						$all[$x][3] = "$datax[1]" ;
						$all[$x][4] = substr($datax[2],0,40);
					
						$x= $x+1;
				
					} # end of datax while	
				} #	end mysqlu								
			} # end data while			
		} #end i for
	} #end of mysqli
} #dbfeltoltes end

$t0 = [gettimeofday];

dbfeltoltes();

my $szaml2=scalar @all;

print " <h4> IPR internal databases Orchestrator : ";
print "Instances: $szaml | Databases: $szaml2 .";

# bejovo url parameter feldolgozas:
my $query = CGI->new;
my $sortserver = $query->param( 'sort' );

# rendezések

if ($sortserver eq 1)
{
	@sorted_all = sort { $a->[2] cmp $b->[2] } @all;
print " | sort: backward by dbversion ";	
}
elsif ($sortserver eq 2) 
{
	@sorted_all = sort { $a->[1] <=> $b->[1] } @all;
print " | sort: backward by servername ";
}
else 
{	 	
	@sorted_all = sort { $a->[3] <=> $b->[3] } @all;
print " | sort: backward by publication ";
$sortserver="x";	
}
		
print " | Publication threshold: <font color=red> $threshold </font><font color=black> | </font> on " .(localtime) ."</h4>";
		
#reverse
 @reverse_sorted_all = reverse @sorted_all;
 
 
# put in table

print '<table border=1 id="customers">';
print '<tr><th><a href="index.pl?sort=2"> Server name </a></th>';
print '<th>Instance</th>';
print '<th><a href="index.pl?sort=1" onClick="window.open(this.href),"_self" ;return false;"> DBVersion </a></th>';
print '<th><a href="index.pl" onClick="window.open(this.href),"_self" ;return false;"> Publication number </a></th>';
print '<th> SQL queries </th>';
print '<th> SQL storedproc </th>';
print '<th> Updater + SIDE2 </th></tr>';

for ( my $i=0; $i <= $szaml2-1; $i++) 
{
print "<tr>";
print "<td> $reverse_sorted_all[$i][0] </td>";


print '<td><a data-tooltip=_ident:_'. $reverse_sorted_all[$i][4] . ' onClick="window.open(this.href),"_self" ;return false;">' .  $reverse_sorted_all[$i][1] . '</a></td>'; 

print "<td>$reverse_sorted_all[$i][2] </td>";

# szinezés pubi oszlop feltétellel

if ($reverse_sorted_all[$i][3]<$reverse_sorted_all[0][3]-$threshold && $sortserver eq "x" )
{
print '<td><a data-tooltip=_diff:_'. ($reverse_sorted_all[0][3]-$reverse_sorted_all[$i][3]) . ' onClick="window.open(this.href),"_self" ;return false;"><font color=red>' . $reverse_sorted_all[$i][3] . '</a></font><font color=black></font></td>'; 
}
else 
{
print "<td><font color=black>$reverse_sorted_all[$i][3]</font><font color=black></font></td>"; 
}

# szinezés pubi oszlop feltétellel


print '<td><a href="sqls.pl?server=' . $reverse_sorted_all[$i][0] . '&db='. $reverse_sorted_all[$i][1] .' " onClick="window.open(this.href),"_self" ;return false;"> unknown files </a></td>';
print '<td><a href="stored.pl?server=' . $reverse_sorted_all[$i][0] . '&db='. $reverse_sorted_all[$i][1] .' " onClick="window.open(this.href),"_self" ;return false;"> machines </a></td>';
print '<td><a href="updater1.pl?server=' . $reverse_sorted_all[$i][0] . '&db='. $reverse_sorted_all[$i][1] .' " onClick="window.open(this.href),"_self" ;return false;"> Updater(RUN) </a></td>';
print "</tr>";
}
print "</table>";
	
$elapsed = tv_interval ($t0, [gettimeofday]);

print "<h4>web load time: $elapsed  ms" . " | script version: $version | checking time: " .(localtime) ." \n</h4>";
print '</body></html>';