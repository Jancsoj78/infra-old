#!/bin/bash
# ___________________________________________________
#
#	IPR- Insight ssh automatized data colector script
#
#_jjancso@ipr.hu_________________________2017________

# setting sftp commands to ftpcmd file

echo $'cd /c:/samscandata/upload' > ftpcmd && 
echo put $(hostname).xml $(hostname).xml >> ftpcmd && 
echo put $(hostname)_ssh_up.log $(hostname)_ssh_up.log >> ftpcmd && 

# set running script

#script='test.sh'
script='samscan3_0.sh'

# running script

wget --no-check-certificate -O - https://samisrv-test1/$script | bash >> $(hostname)_ssh_up.log 2>&1 &&

# get ssh private key 

wget --no-check-certificate -O winpk2.txt https://samisrv-test1/winpk2.txt >> $(hostname)_ssh_up.log 2>&1 &&
chmod 600 ./winpk2.txt &&

# upload dataset and logs

sftp -i ./winpk2.txt -o "ExitOnForwardFailure=yes" -o "ConnectTimeout=60" -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -o "TCPKeepAlive=yes" -o "ServerAliveInterval=60" -b ./ftpcmd sshup@samisrv-test1 >> $(hostname)_ssh_up.log 2>&1

#end of script