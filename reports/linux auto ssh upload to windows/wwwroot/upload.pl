#!/usr/bin/perl -w

use strict;
use CGI;
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;

my $upload_dir = "C:/samscandata/upload";

$CGI::POST_MAX = 1024 * 5000;
my $safe_filename_characters = "a-zA-Z0-9_.-";

my $query = new CGI;
my $filename = $query->param("dataset");

if ( !$filename )
{
 print $query->header ( );
 print "Roger";
 exit;
}

my @nametmp = split(/\\/, $filename);
$filename = $nametmp[$#nametmp];
$filename =~ tr/ /_/;
$filename =~ s/[^$safe_filename_characters]//g;

if ( $filename =~ /^([$safe_filename_characters]+)$/ )
{
 $filename = $1; 
} 
else 
{ 
 die "Filename contains invalid characters"; 
} 
 
my $upload_filehandle = $query->upload("dataset"); 

open ( UPLOADFILE, ">$upload_dir/$filename" ) or die "$!";
binmode UPLOADFILE;

while ( <$upload_filehandle> )
{ 
 print UPLOADFILE; 
} 

close UPLOADFILE; 

# my $mode = 0777;
# chmod $mode, "$upload_dir/$filename";

print $query->header ( ); 
print <<END_HTML;
Uploading succeed
END_HTML
